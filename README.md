Space Game
Daniel Nael, Raigo K�vask, Paul Liibert
Computer Graphics
University of Tartu
2018

How to run?

In order to run Space Game Unity (version 2018.2.14f1) software is needed.
Open up the base folder in Unity and you are set to go. We recommend you to open MainScene.
Control the spaceship with WASDQE and use mouse left and right buttons to shoot projectiles and lasers.