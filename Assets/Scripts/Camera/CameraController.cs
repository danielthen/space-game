﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Camera controller.
/// </summary>
public class CameraController : MonoBehaviour 
{
	//Main variables
	[SerializeField] private float cameraSpeed;
	[SerializeField] private Transform target;

	void Awake()
	{
		if (target == null) 
		{
			Debug.LogWarning ("There isn't any target to follow.");
			return;
		}
	}

	void FixedUpdate()
	{
		FollowTarget ();
	}

	/// <summary>
	/// Follows the target.
	/// </summary>
	private void FollowTarget()
	{
		transform.position = Vector3.Lerp(transform.position, target.position, cameraSpeed * Time.deltaTime);
		transform.rotation = Quaternion.Lerp (transform.rotation, target.rotation, cameraSpeed * Time.deltaTime);
	}
}
