﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Planet : MonoBehaviour 
{
	public Shield shield{get; set;}
	[SerializeField] private Image healthBar;

	[SerializeField] private float rotationSpeed; 

	void Start()
	{
		this.shield = GetComponentInChildren<Shield> ();
	}

	void Update()
	{
		transform.Rotate (Vector3.up * rotationSpeed * Time.deltaTime);
	}

	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "Asteroid" || col.gameObject.tag == "Enemy") 
		{
			GetComponent<Health> ().Damage (10f);
			col.gameObject.GetComponent<Health> ().Die ();
		}
	}

	/// <summary>
	/// If planet gets hurt then update planet's healthbar.
	/// </summary>
	/// <param name="fillAmount">Fill amount.</param>
	public void UpdateHealthBar(float fillAmount)
	{
		healthBar.fillAmount = fillAmount;
	}
}