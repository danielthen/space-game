﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour 
{
	private float moveSpeed;
	private Vector3 moveDirection;

	void Start()
	{
		moveSpeed = Random.Range (1, 10f);
		moveDirection = new Vector3 (Random.Range(0, 2), Random.Range(0, 2), Random.Range(0, 2));
	}

	void Update()
	{
        Vector3 pos = transform.position;
        if (Vector3.Distance(pos, new Vector3(0, 0, 0)) > 800)
        {
            AsteroidManager asteroidManager = GameObject.FindGameObjectWithTag("EnemyManager").GetComponent<AsteroidManager>();
            asteroidManager.Remove(this.gameObject);
        }
        Move ();
	}

	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "SpaceEnd") 
		{
			GameObject.FindGameObjectWithTag ("EnemyManager").GetComponent<AsteroidManager> ().Remove (gameObject);
		}
		if (col.gameObject.tag == "Playnet") 
		{
			GameObject.FindGameObjectWithTag ("EnemyManager").GetComponent<AsteroidManager> ().Remove (gameObject);
		}
	}

	/// <summary>
	/// Move this instance.
	/// </summary>
	void Move()
	{
		transform.Translate (moveDirection * moveSpeed * Time.deltaTime);
	}
}
