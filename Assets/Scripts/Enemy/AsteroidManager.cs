﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidManager : MonoBehaviour {
    [SerializeField] private GameObject asteroidPrefab;
    public List<GameObject> asteroids;

    public int NumberOfAsteroids { get; set; }

    private void Awake()
    {
        NumberOfAsteroids = 500;
		for (int i = 0; i < NumberOfAsteroids; i++) 
		{
			SpawnAsteroid();
		}
    }

    private void SpawnAsteroid()
    {

        GameObject enemy = Instantiate(asteroidPrefab, Position(transform.position), Quaternion.identity);
        enemy.GetComponent<NoiseBall.NoiseBallRenderer>().Radius = Random.Range(4.0f, 8.0f);
        enemy.GetComponent<NoiseBall.NoiseBallRenderer>().NoiseAmplitude = Random.Range(0.0f, 1.0f);
        enemy.GetComponent<NoiseBall.NoiseBallRenderer>().NoiseFrequency = Random.Range(0.0f, 1.0f);
        enemy.GetComponent<NoiseBall.NoiseBallRenderer>().SurfaceColor = new Color(Random.Range(0.3f,1.0f), Random.Range(0.4f, 1.0f), Random.Range(0.6f, 1.0f), 0.3f);
        asteroids.Add(enemy);

    }
    private Vector3 Position(Vector3 center)
    {
        Vector3 spawnPosition = Random.onUnitSphere * (Random.Range(500,1000) + 1 * 0.5f) + center;
        return spawnPosition;
    }
    public void Remove(GameObject asteroid)
    {
        asteroids.Remove(asteroid);
        Destroy(asteroid);
    }

}
