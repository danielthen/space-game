﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour 
{
	// Main variables.
	[SerializeField] private float moveSpeed;
	public EnemyManager enemyManager{ get; set;}
	//private int currentTarget;
	private Vector3 targetPos = Vector3.zero;
	//private bool canChangeTarget;
	//private Rigidbody rBody;
	private Image healthBar;
	private bool startAttack;
	private GameObject projectilePrefab;
	private float shootCoolDownTime;
	private float currentCoolDownTime;
	private GameObject target;
	private AudioPlayer audioPlayer;

	void Start()
	{
		currentCoolDownTime = 0f;
		shootCoolDownTime = Random.Range (0f, 2f);
		projectilePrefab = Resources.Load ("Projectile 1") as GameObject;
		startAttack = false;
		moveSpeed = 1f;
		//SetTargetPosition ();
		//canChangeTarget = true;
		moveSpeed -= Random.Range (0f, 0.5f);
		//SetTargetPosition ();
		//rBody = GetComponent<Rigidbody> ();
		healthBar = GetComponentInChildren<Image> ();
		int[] oneAndMinusOne = new int[]{ -1, 1 };
		targetPos = new Vector3 (enemyManager.Planet.transform.position.x + oneAndMinusOne[Random.Range(0, 2)] * Random.Range(100, 400), 
			enemyManager.Planet.transform.position.y + oneAndMinusOne[Random.Range(0, 1)] * Random.Range(100, 400), 
			enemyManager.Planet.transform.position.z + oneAndMinusOne[Random.Range(0, 2)] * Random.Range(100, 400));
		int randomNo = Random.Range (-5, 5);
		target = (randomNo > 0) ? enemyManager.Planet : enemyManager.player;
		audioPlayer = GetComponent<AudioPlayer> ();
	}

	void FixedUpdate()
	{
		if (Pause.Dead || Pause.Paused) return;
		if(startAttack) Shoot ();
		Move ();
	}


	/// <summary>
	/// Move enemy.
	/// </summary>
	private void Move()
	{
		if (startAttack) 
		{
			transform.LookAt (target.transform.position);
			return;
		}

		if (Vector3.Distance(transform.position, targetPos) < 0.1f) 
		{
			//canChangeTarget = true;
			startAttack = true;
			return;
		}
		transform.LookAt (targetPos);
		transform.position = Vector3.Lerp(transform.position, targetPos, moveSpeed * Time.deltaTime);
		//transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
	}

	private void Shoot()
	{
		currentCoolDownTime += Time.deltaTime;
		if (currentCoolDownTime > shootCoolDownTime) 
		{
			audioPlayer.PlayAudio (0);
			GameObject go = Instantiate (projectilePrefab, transform.position, transform.rotation);
			Destroy (go, 5f);
			currentCoolDownTime = 0f;
			shootCoolDownTime = Random.Range (0f, 2f);
		}
	}

	/*
	/// <summary>
	/// Changes the target.
	/// </summary>
	private IEnumerator changeTarget()
	{
		SetTargetPosition ();
		canChangeTarget = false;
		yield return new WaitForSeconds (Random.Range(5f, 10f));
		GetComponent<Rigidbody> ().AddRelativeForce (Vector3.forward * moveSpeed);
		canChangeTarget = true;
	}

	/// <summary>
	/// Sets the target position with some randomness.
	/// </summary>
	private void SetTargetPosition()
	{
		Vector3 targetRealPos = enemyManager.enemyMovePositions [Random.Range (0, enemyManager.enemyMovePositions.Count - 1)].position;
		targetPos = new Vector3 (targetRealPos.x + Random.Range(-10, 10), targetRealPos.y + Random.Range(-10, 10), targetRealPos.z + Random.Range(-10, 10));
		transform.LookAt (targetPos);
	}
	*/

	/// <summary>
	/// Updates the health bar.
	/// </summary>
	/// <param name="fillAmount">Fill amount.</param>
	public void UpdateHealthBar(float fillAmount)
	{
		healthBar.fillAmount = fillAmount;
	}

	/// <summary>
	/// Hides healthbar.
	/// </summary>
	public void HideHealthBar()
	{
		if(healthBar.color.a > 0f) healthBar.color = new Color(healthBar.color.r, healthBar.color.g, healthBar.color.b, 0f);
	}

	/// <summary>
	/// Show healthbar.
	/// </summary>
	public void ShowHealthBar()
	{
		if(healthBar.color.a > 0f) healthBar.color = new Color(healthBar.color.r, healthBar.color.g, healthBar.color.b, 1f);
	}
}
