﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour 
{
	public GameObject[] enemySpawnPositions;
	//public GameObject[] enemyAttackPositions;
	public List<GameObject> enemies;
	[SerializeField] private GameObject enemyPrefab;
	[SerializeField] private float spawnCooldown;
	public int NumberOfEnemies{get; set;}
	private int numberOfEnemiesSpawned;
	private int numberOfEnemiesDefeated;
	public GameObject Planet;
	public GameObject player;
	[SerializeField] private Game game;

	private float coolDownTime;
	private float currentCoolDownTime;

	private enum EnemyManagerState { SPAWN, NEW_WAVE, IDLE }

	private EnemyManagerState emState;

	private Animator spawnAnim;

	void Awake ()
	{
		spawnAnim = GetComponentInChildren<Animator> ();
		coolDownTime = 1f + Random.Range (0, 1f);
		currentCoolDownTime = 0;
		emState = EnemyManagerState.NEW_WAVE;
		enemySpawnPositions = GameObject.FindGameObjectsWithTag ("EnemySpawnLocation");
		//enemyAttackPositions = GameObject.FindGameObjectsWithTag ("EnemyAttackPoint");
		NumberOfEnemies = 1;
		numberOfEnemiesSpawned = 0;
		numberOfEnemiesDefeated = 0;
	}

	void Update()
	{
		if (Pause.Dead || Pause.Paused) return;
		currentCoolDownTime += Time.deltaTime;
		switch (emState) 
		{
			case EnemyManagerState.IDLE:
				if (numberOfEnemiesDefeated == NumberOfEnemies) 
				{
					numberOfEnemiesSpawned = 0;
					numberOfEnemiesDefeated = 0;
					NumberOfEnemies++;
					emState = EnemyManagerState.NEW_WAVE;
					break;
				}
				break;
			case EnemyManagerState.SPAWN:
				if (currentCoolDownTime < coolDownTime)
					return;
				if (numberOfEnemiesSpawned == NumberOfEnemies) {
					numberOfEnemiesSpawned = 0;
					emState = EnemyManagerState.IDLE;
					break;
				}
				SpawnEnemy ();
				currentCoolDownTime = 0f;
				break;
			case EnemyManagerState.NEW_WAVE:
				game.NewWave ();
				emState = EnemyManagerState.SPAWN;
				break;
		}
	}

	/// <summary>
	/// Creates and spawns the enemy prefab.
	/// </summary>
	private void SpawnEnemy()
	{
		if (enemySpawnPositions.Length <= 0) 
		{
			Debug.LogWarning ("There are no enemy spawn locations.");
			return;
		}

		Vector3 spawnPosition = enemySpawnPositions [Random.Range (0, enemySpawnPositions.Length - 1)].transform.position;

		GameObject enemy = Instantiate (enemyPrefab, spawnPosition, Quaternion.identity);
		enemy.GetComponent<Enemy> ().enemyManager = this;
		enemies.Add (enemy);

		numberOfEnemiesSpawned++;
		spawnAnim.SetTrigger ("Spawn");
	}

	/// <summary>
	/// Remove the specified enemy.
	/// </summary>
	/// <param name="enemy">Enemy.</param>
	public void Remove(GameObject enemy)
	{
		enemies.Remove (enemy);
		numberOfEnemiesDefeated++;
	}
}
