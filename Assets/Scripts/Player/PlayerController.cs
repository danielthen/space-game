﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Player controller.
/// </summary>
public class PlayerController : MonoBehaviour 
{
	// Components
	private Rigidbody rBody;
	private AudioPlayer audioPlayer;

	// Player's look and move
	// Move() method variables.
	[SerializeField] private float moveSpeed;
	private Vector3 moveDirection = Vector3.zero;

	//Look() method variables.
	[SerializeField] private float mouseSensitivity;
	private float xAxisClamp;

	//VFX
	public ParticleSystem[] thrusters;

	void Start()
	{
		LockMouse ();
		InitializeAllComponents ();
	}

	public void FixedUpdate()
	{
		Move ();
		if (Pause.Paused || Pause.Dead) return;
		Look ();
	}

	/// <summary>
	/// Initializes all components and start values.
	/// </summary>
	private void InitializeAllComponents()
	{
		rBody = GetComponent<Rigidbody> ();
		audioPlayer = GetComponent<AudioPlayer> ();
	}

	/// <summary>
	/// Moves the player.
	/// </summary>
	private void Move()
	{
		moveDirection = new Vector3 (Input.GetAxis ("Horizontal"), Input.GetAxis("Upwards"), Input.GetAxis ("Vertical"));

		if (Pause.Paused || Pause.Dead) moveDirection = new Vector3 (0f, 0f, 0f);

		if (moveDirection.z > 0)
		{
			audioPlayer.ChangeAudioSourcePitch (1f);
			PlayThrusters ();
			audioPlayer.PlayAudioLooping (0);
		} 
		else 
		{
			StopThrusters ();
			audioPlayer.StopAudio ();
		}
		moveDirection = transform.TransformDirection (moveDirection);

		if (Input.GetKey (KeyCode.LeftShift)) 
		{
			audioPlayer.ChangeAudioSourcePitch (3f);
			moveDirection *= 10;
		}
		rBody.velocity = moveDirection * moveSpeed * Time.deltaTime;
	}

	/// <summary>
	/// Mouse look controls.
	/// </summary>
	private void Look()
	{
		// Some ideas by Acacia Developer from YouTube.
		// https://www.youtube.com/watch?v=RlXxDfiy-J8

		float mouseX = Input.GetAxis("Mouse X");
		float mouseY = Input.GetAxis("Mouse Y");

		float rotAmountX = mouseX * mouseSensitivity * Time.deltaTime;
		float rotAmountY = mouseY * mouseSensitivity * Time.deltaTime;

		xAxisClamp -= rotAmountY;

		Vector3 targetRot = transform.rotation.eulerAngles;

		targetRot.x -= rotAmountY;
		targetRot.z = 0;
		targetRot.y += rotAmountX;

		// Set the boundaries of camera rotation.
		if (xAxisClamp > 90) 
		{
			xAxisClamp = 90;
			targetRot.x = 90;
		} 
		else if (xAxisClamp < -90) 
		{
			xAxisClamp = -90;
			targetRot.x = 270;
		}

		transform.rotation = Quaternion.Euler (targetRot);
	}

	/// <summary>
	/// Locks the mouse.
	/// </summary>
	private void LockMouse()
	{
		Cursor.lockState = CursorLockMode.Locked;
	}

	/// <summary>
	/// Play thrusters VFX.
	/// </summary>
	private void PlayThrusters()
	{
		foreach (ParticleSystem p in thrusters) 
		{
			p.Play ();
		}
	}

	/// <summary>
	/// Play thrusters VFX.
	/// </summary>
	private void StopThrusters()
	{
		foreach (ParticleSystem p in thrusters) 
		{
			p.Stop ();
		}
	}

	void OnCollisionEnter(Collision col)
	{
		Health colHealth = col.gameObject.GetComponent<Health> ();
		if (colHealth != null) 
		{
			colHealth.Damage (10);
		}

		gameObject.GetComponent<Health> ().Die ();
	}
}
