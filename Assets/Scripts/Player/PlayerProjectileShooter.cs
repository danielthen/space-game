﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectileShooter : MonoBehaviour 
{
	// Main variables.
	private GameObject projectilePrefab;
	[SerializeField] private GameObject playerCamera;
	[SerializeField] private float shootRange;
	[SerializeField] private float laserDamage;
	private LineRenderer laser;
	private AudioPlayer audioPlayer;

	void Start()
	{
		projectilePrefab = Resources.Load ("Projectile") as GameObject;
		laser = GetComponent<LineRenderer> ();
		laser.positionCount = 2;
		audioPlayer = GetComponent<AudioPlayer> ();
	}

	void Update()
	{
		if (Pause.Paused || Pause.Dead) return;
		Shoot ();
	}

	/// <summary>
	/// Shoots this the projectile prefab.
	/// </summary>
	private void Shoot()
	{
		if (Input.GetMouseButtonDown (0)) 
		{
			GameObject go = Instantiate (projectilePrefab, transform.position, transform.rotation);
			Destroy (go, 5f);
			audioPlayer.PlayAudio (0);
		} 

		if (Input.GetMouseButton (1)) 
		{
			audioPlayer.PlayAudio (0);
			RaycastHit hit;
			if (Physics.Raycast (transform.position, playerCamera.transform.forward, out hit, shootRange)) 
			{
				laser.enabled = true;
				laser.SetPosition (0, transform.position);
				laser.SetPosition (1, hit.point);

				if (hit.transform.gameObject.GetComponent<Health> () != null) 
				{
					hit.transform.gameObject.GetComponent<Health> ().Damage (laserDamage);
				}
			} 
			else 
			{
				laser.enabled = true;
				laser.SetPosition (0, transform.position);
				laser.SetPosition (1, transform.forward * shootRange + transform.position);

				if (hit.transform != null) 
				{
					if (hit.transform.gameObject.GetComponent<Health> () != null) 
					{
						hit.transform.gameObject.GetComponent<Health> ().Damage (laserDamage);
					}
				}
			}
		} 
		else 
		{
			laser.enabled = false;
		}
	}
}
