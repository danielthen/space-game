﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetHit : MonoBehaviour {
    int count = 0;
    float [] points;
	// Use this for initialization
	void Start () {
        points = new float[1000];
        updateMaterial();
	}
	
    private void OnCollisionEnter(Collision collision) {
        if(count > 330) {
            return;
        }
        Vector3 hitPoint = collision.contacts[0].point;
        //Debug.Log("Collision point: " + hitPoint);
        Vector3 relativePoint = transform.InverseTransformPoint(hitPoint);
        points[count * 3 + 0] = relativePoint.x;
        points[count * 3 + 1] = relativePoint.y;
        points[count * 3 + 2] = relativePoint.z;
        count += 1;
        updateMaterial();
    }
    private void updateMaterial() {
        gameObject.GetComponentInChildren<MeshRenderer>().material.SetFloatArray("_hitPoints", points);
		gameObject.GetComponentInChildren<MeshRenderer>().material.SetInt("_hitCount", count);
    }
}