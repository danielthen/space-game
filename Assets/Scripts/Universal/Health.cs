﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour 
{
	[SerializeField] private float healthValue;
	public float MaxHealthValue{get; set;}
	private GameObject explosion;

	void Awake()
	{
		if (healthValue <= 0f) healthValue = 20f;
		MaxHealthValue = healthValue;
		explosion = Resources.Load ("Explosion") as GameObject;
	}

	/// <summary>
	/// Damages the gameobject with specified amount.
	/// </summary>
	/// <param name="amount">Damage amount.</param>
	public void Damage(float amount)
	{
		healthValue -= amount;
		if (healthValue <= 0f) 
		{
			Die();
		}

		if (gameObject.tag == "Planet") 
		{
			GetComponent<Planet> ().UpdateHealthBar (healthValue / MaxHealthValue);
		}
		else if (gameObject.tag == "Enemy") 
		{
			GetComponent<Enemy> ().UpdateHealthBar (healthValue / MaxHealthValue);
		}
	}

	/// <summary>
	/// Kills this instance.
	/// </summary>
	public void Die()
	{
		if (gameObject.tag == "Enemy") 
		{
			AudioPlayer.PlayExplosionAudio ();
			EnemyManager enemyManager = GameObject.FindGameObjectWithTag ("EnemyManager").GetComponent<EnemyManager> ();
			enemyManager.Remove (this.gameObject);
		} 
		else if (gameObject.tag == "Asteroid") 
		{
			AudioPlayer.PlayExplosionAudio ();
			AsteroidManager asteroidManager = GameObject.FindGameObjectWithTag ("EnemyManager").GetComponent<AsteroidManager> ();
			asteroidManager.Remove (this.gameObject);
		} 
		else if (gameObject.tag == "Planet") 
		{
			AudioPlayer.PlayExplosionAudio ();
			if (Pause.Dead) return;
			gameObject.GetComponent<Animator> ().SetTrigger ("Death");
			GameObject.FindGameObjectWithTag ("GameController").GetComponent<Game> ().EndGame ();
			return;
		}
		else if (gameObject.tag == "Player") 
		{
			AudioPlayer.PlayExplosionAudio ();
			if (Pause.Dead) return;
			gameObject.GetComponentInParent<Animator> ().SetTrigger ("Death");
			GameObject.FindGameObjectWithTag ("GameController").GetComponent<Game> ().EndGame ();
			GameObject exp1 = Instantiate (explosion, transform.position, Quaternion.identity);
			Destroy (exp1, 2f);
			return;
		}

		GameObject exp = Instantiate (explosion, transform.position, Quaternion.identity);
		Destroy (exp, 2f);
		Destroy (gameObject);
	}
}
