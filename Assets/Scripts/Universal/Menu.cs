﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour 
{
	private Animator anim;
	[SerializeField] private Text scoreText;

	void Start()
	{
		anim = GetComponent<Animator> ();
		SetTime ();
	}

	void Update()
	{
		if (Input.GetKey(KeyCode.Space))
		{
			anim.SetTrigger ("StartGame");
		}
	}

	public void ExitGame()
	{
		Application.Quit();
	}


	/// <summary>
	/// Sets the scoreText time.
	/// </summary>
	private void SetTime()
	{
		System.TimeSpan time = System.TimeSpan.FromSeconds(PlayerPrefs.GetInt("score"));

		string seconds = (time.Seconds < 10) ? "0" + time.Seconds : time.Seconds + "";
		string minutes = (time.Minutes < 10) ? "0" + time.Minutes : time.Minutes + "";
		string hours = (time.Hours < 10) ? "0" + time.Hours : time.Hours + "";

		scoreText.text = string.Format("{0}:{1}:{2}", hours, minutes, seconds);
	}
}
