﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour 
{
	[SerializeField] private int power;

	void Start()
	{
		if (power <= 0) power = 10;
	}

	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "Asteroid" || col.gameObject.tag == "Enemy") 
		{
			Damage (10);
			col.gameObject.GetComponent<Health> ().Die ();
		}
	}

	/// <summary>
	/// Make damage to shield.
	/// </summary>
	/// <param name="amount">Amount to make damage.</param>
	public void Damage(int amount)
	{
		power -= amount;
		if (power <= 0)
			Deactivate ();
	}

	/// <summary>
	/// Deactivate this instance.
	/// </summary>
	public void Deactivate()
	{
		this.gameObject.GetComponent<SphereCollider> ().enabled = false;
		this.gameObject.GetComponent<MeshRenderer> ().enabled = false;
	}

	/// <summary>
	/// Activate this instance.
	/// </summary>
	public void Activate()
	{
		this.gameObject.GetComponent<SphereCollider> ().enabled = true;
		this.gameObject.GetComponent<MeshRenderer> ().enabled = true;
	}
}
