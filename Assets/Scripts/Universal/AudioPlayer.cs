﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour 
{
	private AudioSource audioSource;
	private static AudioPlayer expAudio;

	[SerializeField] private List<AudioClip> audioClips;

	void Start () 
	{
		audioSource = GetComponent<AudioSource> ();
		expAudio = GameObject.FindGameObjectWithTag ("ExplosionAudio").GetComponent<AudioPlayer>();
	}

	/// <summary>
	/// Play audio clip.
	/// </summary>
	/// <param name="audioClip">Audio clip.</param>
	public void PlayAudio(int audioClip)
	{
		if (audioSource == null) return;
		if (audioClips != null) 
		{
			audioSource.clip = audioClips [audioClip];
			audioSource.pitch = Random.Range (0.9f, 1.1f);
			audioSource.Play ();
		}
	}

	/// <summary>
	/// Play audio clip looping.
	/// </summary>
	/// <param name="audioClip">Audio clip.</param>
	public void PlayAudioLooping(int audioClip)
	{
		if (audioSource.isPlaying) return;
		audioSource.clip = audioClips [audioClip];
		//audioSource.pitch = 3f;
		audioSource.loop = true;
		audioSource.Play ();
	}

	/// <summary>
	/// Stops the audio playback.
	/// </summary>
	public void StopAudio()
	{
		audioSource.loop = false;
		audioSource.Stop ();
	}

	/// <summary>
	/// Changes the audio source pitch.
	/// </summary>
	/// <param name="val">Value.</param>
	public void ChangeAudioSourcePitch(float val)
	{
		audioSource.pitch = val;
	}

	/// <summary>
	/// Creates the explosion audio and plays its sound.
	/// </summary>
	public static void PlayExplosionAudio()
	{
		expAudio.PlayAudio (0);
	}
}