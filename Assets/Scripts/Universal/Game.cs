﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour 
{
	private int score;
	[SerializeField] private Text scoreText;
	private bool gamePaused;
	[SerializeField] private Animator anim;
	[SerializeField] private Text gameInfoText;
	private float playedTime;
	private int wave;
	[SerializeField] private EnemyManager em;
	private AudioPlayer audioPlayer;

	void Start()
	{
		playedTime = 0f;
		Pause.Paused = false;
		gamePaused = false;
		Pause.Dead = false;
		Cursor.visible = false;
		wave = 0;
		audioPlayer = GetComponent<AudioPlayer> ();
	}

	void Update()
	{
		if (Pause.Dead) 
		{
			if (Input.GetKeyDown (KeyCode.Space))
			{
				anim.SetTrigger ("End");
			}

			return;
		}

		if (Input.GetKeyDown (KeyCode.Escape))
			PauseGame();
		
		if (Pause.Paused) return;
		SetTime ();
	}

	/// <summary>
	/// Sets the scoreText time.
	/// </summary>
	private void SetTime()
	{
		playedTime += Time.deltaTime;
		System.TimeSpan time = System.TimeSpan.FromSeconds((int) playedTime);

		string seconds = (time.Seconds < 10) ? "0" + time.Seconds : time.Seconds + "";
		string minutes = (time.Minutes < 10) ? "0" + time.Minutes : time.Minutes + "";
		string hours = (time.Hours < 10) ? "0" + time.Hours : time.Hours + "";

		scoreText.text = string.Format("{0}:{1}:{2}", hours, minutes, seconds);
	}

	/// <summary>
	/// Ends the game.
	/// </summary>
	public void EndGame()
	{
		audioPlayer.PlayAudio (0);
		gameInfoText.text = "GAME OVER";
		anim.SetTrigger ("Death");
		score = (int) playedTime;

		int savedScore = PlayerPrefs.GetInt ("score", 0);
		if (score > savedScore) PlayerPrefs.SetInt ("score", (int) score);
		scoreText.text += "\n PRESS SPACE TO CONTINUE";
		Pause.Dead = true;
	}

	/// <summary>
	/// Pauses the game.
	/// </summary>
	public void PauseGame()
	{
		audioPlayer.PlayAudio (1);
		gamePaused = !gamePaused;
		Pause.Paused = gamePaused;
		Cursor.visible = gamePaused;
		anim.SetBool ("paused", gamePaused);
		gameInfoText.text = "GAME PAUSED";

		if (gamePaused)
		{
			Cursor.lockState = CursorLockMode.None;
		} 
		else 
		{
			Cursor.lockState = CursorLockMode.Locked;
		}
	}

	/// <summary>
	/// Loads menu scene.
	/// </summary>
	public void ExitGame()
	{
		score = (int) playedTime;
		int savedScore = PlayerPrefs.GetInt ("score", 0);
		if (score > savedScore) PlayerPrefs.SetInt ("score", (int) score);
		anim.SetTrigger ("End");
	}

	/// <summary>
	/// Start new wave.
	/// </summary>
	public void NewWave()
	{
		audioPlayer.PlayAudio (1);
		wave++;
		gameInfoText.text = "WAVE " + wave;
		anim.SetTrigger ("NewWave");
	}
}
