﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour 
{
	// Main variables.
	[SerializeField] private float moveSpeed;
	[SerializeField] private int damageAmount;
	private bool isGoingToBeDestroyed;
	public Vector3 TargetPosition {get; set;}
	public GameObject explosionPrefab;

	void Start()
	{
		if (moveSpeed <= 0) moveSpeed = 15f;
		isGoingToBeDestroyed = false;
	}

	void Update()
	{
		if (Pause.Dead || Pause.Paused) return;
		//TransformToTarget();
		Move(Vector3.forward);
	}

	void OnCollisionEnter(Collision col)
	{
		/*if (col != null) 
		{
			TargetPosition = transform.position;
			isGoingToBeDestroyed = true;

			if (explosionPrefab != null) 
			{
				GameObject go = Instantiate (explosionPrefab, transform.position, Quaternion.identity);
				Destroy (go, 1f);
			}

			Destroy (gameObject, 0.5f);

			Health h = col.gameObject.GetComponentInParent<Health> ();

			if(h != null)
			{
				h.Damage (damageAmount);
			}
		}*/

		if (col.gameObject.GetComponent<Health> () != null) 
		{
			col.gameObject.GetComponent<Health> ().Damage (damageAmount);
		}
		else if (col.gameObject.GetComponent<Shield> () != null) 
		{
			col.gameObject.GetComponent<Shield> ().Damage (damageAmount);
		}

		GameObject go = Instantiate(explosionPrefab, transform.position, Quaternion.identity);

		Destroy (go, 1);
		isGoingToBeDestroyed = true;
		AudioPlayer.PlayExplosionAudio ();
		Destroy (gameObject);
	}

	/// <summary>
	/// Moves to the target position.
	/// </summary>
	private void TransformToTarget()
	{
		if (!isGoingToBeDestroyed) Destroy (gameObject, 4f);
		transform.position = Vector3.Lerp(transform.position, TargetPosition, moveSpeed * Time.deltaTime);
	}

	/// <summary>
	/// Move object with the specified direction.
	/// </summary>
	/// <param name="direction">Up, down, left, right, forward, back.</param>
	private void Move(Vector3 direction)
	{
		transform.Translate (direction * moveSpeed * Time.deltaTime);
	}
}
