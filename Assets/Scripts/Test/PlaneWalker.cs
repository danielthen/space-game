﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneWalker : MonoBehaviour {
  
    private void OnCollisionStay(Collision collision) {
        Vector3 hitPoint = collision.contacts[0].point;
        Debug.Log("Collision point: " + hitPoint);
        Vector3 relativePoint = transform.InverseTransformPoint(hitPoint);
        Debug.Log("Local collision point: " + relativePoint);
        gameObject.GetComponent<MeshRenderer>().material.SetVector("_HitPoint", relativePoint);
    }
}
