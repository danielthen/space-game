﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldTest : MonoBehaviour {
    private bool AnimationStarted = false;
    private void OnCollisionEnter(Collision collision) {
        if (!AnimationStarted) {
            Vector3 hitPoint = collision.contacts[0].point;
            Vector3 relativePoint = transform.InverseTransformPoint(hitPoint);
            gameObject.GetComponent<MeshRenderer>().material.SetVector("_HitPoint", relativePoint);
            StartCoroutine(Animate());
        }
    }
    IEnumerator Animate() {
        AnimationStarted = true;
        float a = 0;
        while(a < 1) {
            if( a > 1) {
                a = 1;
            }
            a += 2 * Time.deltaTime;
            gameObject.GetComponent<MeshRenderer>().material.SetFloat("_Distance", a);
            yield return null;
        }
        AnimationStarted = false;
    }
}
