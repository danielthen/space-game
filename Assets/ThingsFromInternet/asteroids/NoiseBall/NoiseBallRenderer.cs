﻿//author: https://github.com/keijiro/NoiseBall
using UnityEngine;
using UnityEngine.Rendering;

namespace NoiseBall
{
    [ExecuteInEditMode]
    public class NoiseBallRenderer : MonoBehaviour
    {
        #region Exposed Parameters

        [SerializeField]
        NoiseBallMesh _mesh;

        [Space]
        [SerializeField]
        float _radius = 1.0f;

        [SerializeField]
        float _noiseAmplitude = 0.05f;

        [SerializeField]
        float _noiseFrequency = 1.0f;


        [Space]
        [SerializeField, ColorUsage(false)]
        Color _surfaceColor = Color.white;

        [SerializeField, Range(0, 1)]
        float _metallic = 0.5f;

        [SerializeField, Range(0, 1)]
        float _smoothness = 0.5f;

        [Space]
        [SerializeField]
        ShadowCastingMode _castShadows;

        [SerializeField]
        bool _receiveShadows;

        #endregion

        #region Private Resources

        [SerializeField, HideInInspector]
        Shader _surfaceShader;


        #endregion

        #region Private Variables

        Material _surfaceMaterial;
        MaterialPropertyBlock _materialProperties;
        Vector3 _noiseOffset;
        int _direction;
        int _axis;
        #endregion

        #region MonoBehaviour Functions
        private void Awake()
        {
            //Random rnd = new Random();
        
            int choice = Random.Range(0, 2);
            if (choice == 0)
            {
                _axis = Random.Range(0,3);
                _direction = -1;
            
            }
            else
            {
                _axis = Random.Range(0,3);
                _direction = 1;
            }
        }
        void ChangePosition()
        {

        }
        void Update()
        {

            if (_surfaceMaterial == null)
            {
                _surfaceMaterial = new Material(_surfaceShader);
                _surfaceMaterial.hideFlags = HideFlags.DontSave;
            }


            if (_materialProperties == null)
                _materialProperties = new MaterialPropertyBlock();
            /*if (_axis == 0) //x
            {
                transform.position = new Vector3(pos.x+_direction,pos.y,pos.z);
            }
            else if (_axis == 1) //y
            {
                transform.position = new Vector3(pos.x, pos.y+_direction, pos.z);
            }
            else //z
            {
                transform.position = new Vector3(pos.x, pos.y, pos.z+_direction);
            }*/
            _surfaceMaterial.color = _surfaceColor;

            _surfaceMaterial.SetFloat("_Metallic", _metallic);
            _surfaceMaterial.SetFloat("_Glossiness", _smoothness);
            _surfaceMaterial.SetFloat("_Radius", _radius);

            _materialProperties.SetFloat("_NoiseAmplitude", _noiseAmplitude);
            _materialProperties.SetFloat("_NoiseFrequency", _noiseFrequency);
            _materialProperties.SetVector("_NoiseOffset", _noiseOffset);
            Graphics.DrawMesh(
                _mesh.sharedMesh, transform.localToWorldMatrix,
                _surfaceMaterial, 0, null, 0, _materialProperties,
                _castShadows, _receiveShadows, transform
            );


        }

        public float Radius
        {
            get
            {
                return _radius;
            }
            set
            {
                _radius = value;
            }
        }

        public float NoiseAmplitude
        {
            get
            {
                return _noiseAmplitude;
            }
            set
            {
                _noiseAmplitude = value;
            }
        }

        public float NoiseFrequency
        {
            get
            {
                return _noiseFrequency;
            }
            set
            {
                _noiseFrequency = value;
            }
        }

        public Color SurfaceColor
        {
            get
            {
                return _surfaceColor;
            }
            set
            {
                _surfaceColor = value;
            }
        }
        #endregion
    }
}
