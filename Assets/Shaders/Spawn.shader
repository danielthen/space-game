﻿Shader "Raigo/Spawn"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Frequency("Frequency", float) = 10.0
		_Lacunarity("Lacunarity", float) = 2.0
		_Gain("Gain", float) = 0.5
		_Jitter("Jitter", Range(0,1)) = 1.0
		_Radius("Radius", Range(0,0.5)) = 0.5
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite On
		Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			#include "GPUVoronoiNoise2D.cginc"
			#define OCTAVES 1

			float _Radius;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			
			fixed dist(float2 a, float2 b) {
				fixed _x = (a.x - b.x) * (a.x - b.x);
				fixed _y = (a.y - b.y) * (a.y - b.y);
				fixed _d = sqrt(_x + _y);
				return _d;
			}
			
			fixed4 voronoi(float2 uv) {
				float n = voronoi2D(0.5 * float2(uv.x, uv.y), 1);
				return fixed4(n, n, n, n);
			}
			fixed4 voronoiMod(float2 uv) {
				float n =	0.1 * voronoi(uv * 4);
				n		+=	0.8 * voronoi(uv * 1);
				return float4(n * 0.7, n * 0.8, n, n);
			}
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				// just invert the colors
				float dista = dist(i.uv, float2(0.5, 0.5));
				if (dista < _Radius) {
					fixed4 voronois = voronoiMod(i.uv);
					col.rgba = voronois;
					col.a = col.a * (_Radius - dista) / _Radius;
					col += fixed4(1.5, 1.5, 1.5, 1) * _Radius - dista;
				}
				else {
					col.a = 0;
				}
				return col;
			}
			ENDCG
		}
	}
}
