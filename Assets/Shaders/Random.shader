﻿Shader "Raigo/Random" {
	Properties {
	}
	SubShader {
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		LOD 200
		Pass{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#pragma glsl
			#include "Perlin3D.cginc"


			struct Input {
				float2 uv_MainTex;
			};


			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float3 vertex2 : TEXCOORD1;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex2 = v.vertex;
				float4 vert = v.vertex;
				o.vertex = UnityObjectToClipPos(vert);
				return o;
			}
			fixed4 frag(v2f i) : SV_Target
			{
				float nois = 0;
				float3 newvert = float3(i.vertex2.x - _Time.y / 70, i.vertex2.y - _Time.y / 80, i.vertex2.z - _Time.y / 90);
				nois += 0.3 * cnoise(2 * newvert);
				nois += 0.9 * cnoise(8 * newvert);
				nois += 0.4 * cnoise(16 * newvert);
				nois += 0.5 * cnoise(26 * newvert);
				nois += 1 * cnoise(2 * newvert);
				return fixed4(nois, nois, 0.1 + nois, 1);
			}
			
			ENDCG
		}
	}
	FallBack "Diffuse"
}
