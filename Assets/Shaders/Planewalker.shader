﻿Shader "Custom/Planewalker" {
	Properties {
		_LowColor ("Color", Color) = (1,1,1,1)
		_HighColor("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_HitPoint("Base (RGB)", Vector) = (1, 1, 1)
		_Distance("Distance from hit", float) = 1
		_Height("Lift height", float) = 1
		_HeightColor("Height color", float) = 1

		_FrequencyX("X Frequency", float) = 10.0
		_FrequencyY("Y Frequency", float) = 10.0
		_Frequency("Total Frequency", float) = 10.0
		_Lacunarity("Lacunarity", float) = 2.0
		_Gain("Gain", float) = 0.5
		_CutoutThresh("Cutout Threshold", Range(0.0,1.0)) = 0.2
		_Jitter("Jitter", Range(0,1)) = 1.0
		_HitPoint("Base (RGB)", Vector) = (1, 1, 1)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert
		#include "GPUVoronoiNoise2D.cginc"
		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0
		#define OCTAVES 1

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			//float3 vertex;
			float3 origVertex;
			float changed;
		};
		float3 _HitPoint;
		half _Glossiness;
		half _Metallic;
		fixed4 _LowColor;
		fixed4 _HighColor;

		float _FrequencyY;
		float _FrequencyX;
		float _CutoutThresh;
		float _Distance;
		float _Height;
		float _HeightColor;

		fixed dista(float3 a, float3 b) {
			fixed _x = (a.x - b.x) * (a.x - b.x);
			fixed _y = (a.y - b.y) * (a.y - b.y);
			fixed _z = (a.z - b.z) * (a.z - b.z);
			fixed _d = sqrt(_x + _y + _z);
			return _d;
		}
		void vert(inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.uv_MainTex = float2(v.texcoord.xy);
			o.origVertex = v.vertex;
			float dist = dista(v.vertex, _HitPoint);
			o.changed = 0;
			if (dist < _Distance) {
				o.changed = 1;
				float multiplier = 1;
				if (dist > _Distance - _Distance * 0.3) {
					multiplier = 0.95;
					if (dist > _Distance - _Distance * 0.25) {
						multiplier = 0.75;
						if (dist > _Distance - _Distance * 0.2) {
							multiplier = 0.55;
							if (dist > _Distance - _Distance * 0.15) {
								multiplier = 0.35;
								if (dist > _Distance - _Distance * 0.10) {
									multiplier = 0.15;
								}
							}
						}
					}
				}

				fixed4 norm = fixed4(v.normal, 0);
				v.vertex = v.vertex + multiplier * _Height * norm;
			}
			//float cnois = fBm_F0(_CutoutThresh * float3(v.vertex.x, v.vertex.y + _Time.y / 3, v.vertex.z), OCTAVES);
			//fixed4 norm = fixed4(v.normal, 1);
			//v.vertex = v.vertex + norm * cnois / 20;

		}
		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			//fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			float cnois = fBm_F0(_CutoutThresh * float3(IN.origVertex.x, IN.origVertex.y , IN.origVertex.z), OCTAVES);
			fixed4 c = fixed4 (cnois, cnois, cnois, 1);
			if (IN.changed == 0) {
				c = fixed4(_LowColor.r * cnois, _LowColor.g * cnois, _LowColor.b * cnois, 1);
			}
			else {
				c = fixed4(_HighColor.r * cnois, _HighColor.g * cnois, _HighColor.b * cnois, 1);
			}
			//fixed4 c = fixed4(cnois, cnois, cnois, 1);
			// Metallic and smoothness come from slider variables
			o.Albedo = c.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
