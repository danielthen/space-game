﻿Shader "Custom/World" {
	Properties {
		_DestructPercent("Destruction slider", Range(0, 1)) = 0 
		_Color1 ("Height1", Color) = (1,1,1,1)
		_Color2 ("Height2", Color) = (1,1,1,1)
		_Color3 ("Height3", Color) = (1,1,1,1)
		_Color4 ("Height4", Color) = (1,1,1,1)
		_Color5 ("Height5", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_HitPoint("Hit point", Vector) = (1, 1, 1)


		// Voronoi noise parameters
		_CutoutThresh("Cutout Threshold", Range(0.0,1.0)) = 0.2
		_Frequency("Frequency", float) = 10.0
		_Lacunarity("Lacunarity", float) = 2.0
		_Gain("Gain", float) = 0.5
		_Jitter("Jitter", Range(0,1)) = 1.0

		_Distance("Distance from point", Range(0, 1)) = 0.1
		_BurnArea("Burn area", Range(0, 1)) = 0.1
		_DmgCol("Damage color", Color) = (1,1,1,1)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert
		#include "Perlin3D.cginc"
		#include "GPUVoronoiNoise2D.cginc"
		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			float3 origVertex;
			float voronoi;
			float prlin;
			float3 camera: TEXCOORD2;
		};
		float _DestructPercent;

		float _Distance;
		float _BurnArea;
		float3 _HitPoint;
		float _CutoutThresh;
		half _Glossiness;
		half _Metallic;
		fixed4 _Color1;
		fixed4 _Color2;
		fixed4 _Color3;
		fixed4 _Color4;
		fixed4 _Color5;
		fixed4 _DmgCol;

		int _hitCount = 0;
		float _hitPoints[1000];
		float calcNoise(float3 vert) {
			float cnois = 0;
			cnois += 1 * cnoise(2 * vert);
			cnois += 1 * cnoise(4 * vert);
			cnois += 1 * cnoise(8 * vert);
			cnois += 0.6 * cnoise(14 * vert);
			cnois += 0.5 * cnoise(16 * vert);
			cnois += 0.5 * cnoise(26 * vert);
			cnois += 0.6 * cnoise(60 * vert);
			return cnois;
		}
		float getDist(float3 a, float3 b) {
			fixed _x = (a.x - b.x) * (a.x - b.x);
			fixed _y = (a.y - b.y) * (a.y - b.y);
			fixed _z = (a.z - b.z) * (a.z - b.z);
			return sqrt(_x + _y + _z);
		}
		float3 getPoint(float startInd) {
			return float3(_hitPoints[startInd], _hitPoints[startInd + 1], _hitPoints[startInd + 2]);
		}
		float dista(float3 a, float3 b) {
			float _d = 10000;
			for (int i = 0; i < _hitCount; i++) {
				float _dd = getDist(a, getPoint(i * 3));
				if (_dd < _Distance) {
					return _dd;
				}
			}
			return _d;
		}

		void vert(inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.camera = normalize(mul(unity_WorldToObject, _WorldSpaceCameraPos));
			o.origVertex = v.vertex;
			float dist = dista(v.vertex, _HitPoint);

			fixed4 norm = fixed4(v.normal, 1);
			
			float cnois = calcNoise(v.vertex);
			o.prlin = cnois;
			if(cnois > 0.4) {
				v.vertex = v.vertex + norm * cnois / 20;
			}


			float voronois = fBm_F0(_CutoutThresh * float3(v.vertex.x, v.vertex.y, v.vertex.z), 1);
			o.voronoi = 0;
			if (_DestructPercent > 0) {
				if (1 - cnois % 1 < _DestructPercent) {
					o.voronoi = voronois;
					if (_DestructPercent > voronois) {
						o.voronoi = _DestructPercent;
					}
				}
			}

			if (dist > 0 && dist < _Distance) {
				float modvoronois = 1 - (1 - voronois) * (1 - voronois);
				o.voronoi = (1 - dist) * modvoronois / 10;
				if (voronois > 0.5) {
					o.voronoi = (1 - dist * dist * dist) * modvoronois / 10;
					v.vertex = v.vertex - norm * (1 - dist) * modvoronois / 10;
				}
			}
		}
		fixed4 addGlow(Input i) {
			fixed3 v = normalize(i.camera);
			fixed3 n = normalize(i.origVertex);
			fixed4 glow = pow(1 - abs(dot(v, n)), 2) * 0.9;
			return glow;
		}
		void surf (Input IN, inout SurfaceOutputStandard o) {
			float cnois = IN.prlin;
			// Albedo comes from a texture tinted by color
			fixed4 c = _Color5;
			if (cnois < 0.2) {
				c = _Color1;
			}
			else if (cnois < 0.4) {
				c = _Color2;
			}
			else if (cnois < 0.6) {
				c = _Color3;
			}
			else if (cnois < 0.8) {
				c = _Color4;
			}
			if (IN.voronoi > 0 && cnois > _BurnArea) {
				float4 brown = float4(51, 28, 7, 1);
				c = lerp(c, float4(200, 0, 0, 1), 0.75 * cnois * 0.5);
				c = lerp(c, brown, IN.voronoi * frac(5 * _Time.x - cnois));
			}
			else {
				if (_DestructPercent > 0 && IN.voronoi > 0) {
					float4 brown = float4(51, 28, 7, 1);
					c = lerp(c, float4(200, 0, 0, 1), 0.75 * abs(cnois) * 0.5 * _DestructPercent);
					c = lerp(c, brown, IN.voronoi * frac(5 * _Time.x - cnois) * _DestructPercent);
				}
			}
			fixed4 glow = addGlow(IN);
			o.Albedo = glow.rgb + c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
