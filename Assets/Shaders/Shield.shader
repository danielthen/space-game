﻿Shader "Raigo/shield"
{
	Properties
	{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_HitPoint("Base (RGB)", Vector) = (1, 1, 1)
		_Color("Color", color) = (1,1,1,1)
		_Frequency("Frequency", float) = 10.0
		_Lacunarity("Lacunarity", float) = 2.0
		_Gain("Gain", float) = 0.5
		_CutoutThresh("Cutout Threshold", Range(0.0,1.0)) = 0.2
		_Jitter("Jitter", Range(0,1)) = 1.0

		_TintColor("Tint Color", Color) = (1,1,1,1)
		_Transparency("Transparency", Range(0.0,0.5)) = 0.25
		
		_Distance("Distance", Range(0, 1)) = 0
		_Thickness("Thickness", Range(0.0,0.5)) = 0.5
		_Diameter("Diameter", Range(0, 1)) = 0.1
		
	}
	SubShader
	{
		Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
		LOD 100
		ZWrite On
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			#pragma target 3.0
			#pragma glsl
			#include "GPUVoronoiNoise2D.cginc"
			#define OCTAVES 1

			sampler2D _MainTex;
			fixed4 _Color;

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float3 vertex2 : TEXCOORD1;
				float3 camera: TEXCOORD2;
			};

			float4 _TintColor;
			float3 _HitPoint;
			float _Diameter;
			float _Transparency;
			float _CutoutThresh;
			float _Distance;
			float _Thickness;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex2 = v.vertex;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.camera = normalize(mul(unity_WorldToObject, _WorldSpaceCameraPos));
				return o;
			}
			fixed dista(float3 a, float3 b) {
				fixed _x = (a.x - b.x) * (a.x - b.x);
				fixed _y = (a.y - b.y) * (a.y - b.y);
				fixed _z = (a.z - b.z) * (a.z - b.z);
				fixed _d = sqrt(_x + _y + _z);
				return _d;
			}
			fixed4 addGlow(v2f i, fixed4 col) {
				fixed3 v = normalize(i.camera);
				fixed3 n = normalize(i.vertex2);
				fixed4 glow = pow(1 - abs(dot(v, n)), 1.3) * 0.9;
				return col + glow;
			}
			fixed4 frag(v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col =  _TintColor;
				col.a = _Transparency;
				clip(col.r - _CutoutThresh);
				fixed _d = dista(i.vertex2, _HitPoint);
				
				fixed dist = -0.2 + _Distance * (0.8);
				fixed thick = _Thickness - _Distance * _Thickness;
				fixed midVal = dist - thick / 2;
				if (_d > dist - thick && _d < dist) {
					col.a = abs(_d - dist + 0.25) * 2;
					//col.a = 1;
					/*if (_d > midVal + (thick / 2) * 0.1 || _d < midVal - (thick / 2) * 0.1) {
						col.a = (midVal - (thick / 2)) * _d / midVal;
						col.a = 0.9;
						if (_d > midVal + (thick / 2) * 0.3 || _d < midVal - (thick / 2) * 0.3) {
							col.a = 0.8;
							if (_d > midVal + (thick / 2) * 0.5 || _d < midVal - (thick / 2) * 0.5) {
								col.a = 0.7;
								if (_d > midVal + (thick / 2) * 0.7 || _d < midVal - (thick / 2) * 0.7) {
									col.a = 0.6;
									if (_d > midVal + (thick / 2) * 0.85 || _d < midVal - (thick / 2) * 0.85) {
										col.a = 0.5;
									}
								}
							}
						}
					}*/
				}
				float n = fBm_F0(_CutoutThresh * float3(i.vertex2.x, i.vertex2.y + _Time.y / 3, i.vertex2.z), OCTAVES);
				col.x = n * col.x;
				col.y = n * col.y;
				col.z = n * col.z;
				//return fixed4(-i.camera, 1);
				return addGlow(i, col);
				return addGlow(i, col);
			}
			ENDCG
		}

		}
}
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'
/*
Shader "Raigo/Shield" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader
	{
		Tags {"Queue" = "Transparent" }
		LOD 100

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _TintColor;
			float _Transparency;
			float _CutoutThresh;
			float _Distance;
			float _Amplitude;
			float _Speed;
			float _Amount;




			v2f vert(appdata v)
			{
				v2f o;
				v.vertex.x += sin(_Time.y * _Speed + v.vertex.y * _Amplitude) * _Distance * _Amount;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = fixed4(0.3, 0.3, 0.5, 1);
				//col.a = _Transparency;
				clip(col.r - _CutoutThresh);
				return col;
			}
			ENDCG
		}
	}
}
*/