﻿Shader "Custom/Thruster" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		// Physically based Standard lighting model, and enable shadows on all light types
		Pass{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#pragma glsl
			#include "Perlin3D.cginc"

			sampler2D _MainTex;

			struct Input {
				float2 uv_MainTex;
			};

			half _Glossiness;
			half _Metallic;
			fixed4 _Color;

			// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
			// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
			// #pragma instancing_options assumeuniformscaling
			//UNITY_INSTANCING_BUFFER_START(Props)
				// put more per-instance properties here
			//UNITY_INSTANCING_BUFFER_END(Props)
			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float3 vertex2 : TEXCOORD1;
			};
			fixed dista(float3 a, float3 b) {
				fixed _x = (a.x - b.x) * (a.x - b.x);
				fixed _y = (a.y - b.y) * (a.y - b.y);
				fixed _z = (a.z - b.z) * (a.z - b.z);
				fixed _d = sqrt(_x + _y + _z);
				return _d;
			}
			
			v2f vert(appdata v)
			{
				v2f o;
				o.vertex2 = v.vertex;
				float4 vert = v.vertex;
				float cnois = cnoise(float3(v.vertex.x + _Time.y, v.vertex.y + _Time.y, v.vertex.z));
				fixed _d = dista(float3(v.vertex.xyz), float3(0, 0, 0.5));
				
				float cnois2 = cnoise(16 * float3(v.vertex.x + _Time.y, v.vertex.y + _Time.y, v.vertex.z));

				vert.x = _d * v.vertex.x + (1 - _d) * cnois / 5 + cnois2 / 16;
				vert.y = _d * v.vertex.y + (1 - _d) * cnois / 5 + cnois2 / 16;
				o.vertex = UnityObjectToClipPos(vert);
				return o;
			}
			fixed4 frag(v2f i) : SV_Target
			{
				float3 newvert = float3(i.vertex2.x - _Time.y, i.vertex2.y - _Time.y, i.vertex2.z - _Time.y);
				float cnois = cnoise(10 * newvert);
				return fixed4(0.75 + cnois / 4, 0.5 + cnois / 3, 0, 0.7);
			}
			
			ENDCG
		}
	}
	FallBack "Diffuse"
}
