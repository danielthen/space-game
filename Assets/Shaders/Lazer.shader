﻿Shader "Raigo/Lazer" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		Pass{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#pragma glsl
			#include "Perlin3D.cginc"


			struct Input {
				float2 uv_MainTex;
			};


			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float3 vertex2 : TEXCOORD1;
			};
			fixed dista(float3 a, float3 b) {
				fixed _x = (a.x - b.x) * (a.x - b.x);
				fixed _y = (a.y - b.y) * (a.y - b.y);
				fixed _z = (a.z - b.z) * (a.z - b.z);
				fixed _d = sqrt(_x + _y + _z);
				return _d;
			}

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex2 = v.vertex;
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}
			fixed4 frag(v2f i) : SV_Target
			{
				float3 newvert = float3(i.vertex2.x, i.vertex2.y - _Time.x * 3, i.vertex2.z);
				float cnois = cnoise(10 * newvert);
				return fixed4(0.75 + 0.25 * cnois, 0, 0, 1);
			}

			ENDCG
		}
	}
	FallBack "Diffuse"
}
